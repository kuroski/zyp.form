import copyToClipboard from "copy-text-to-clipboard";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import store from "@/store";
import state from "@/store/state";
import FormBuilder from "@/views/FormBuilder";
import VPage from "@/components/VPage";
import formFactory from "@/factories/formFactory";
import pageFactory from "@/factories/pageFactory";
import sectionFactory from "@/factories/sectionFactory";
import questionFactory from "@/factories/questionFactory";

jest.mock("copy-text-to-clipboard");

describe("FormBuilder", () => {
  const build = ({ overrideState = {} } = {}) => {
    const localVue = createLocalVue();
    localVue.use(Vuex);

    store.replaceState({ ...state, ...overrideState });

    const wrapper = shallowMount(FormBuilder, { store, localVue });

    return {
      wrapper,
      store,
      pages: () => wrapper.findAll(VPage),
      createPageButton: () => wrapper.find(".create-page"),
      exportButton: () => wrapper.find(".export"),
      copyExportButton: () => wrapper.find(".export-copy"),
      importField: () => wrapper.find(".import-field"),
      errorAlert: () => wrapper.find(".error-alert")
    };
  };

  it("renders main component", () => {
    const { wrapper } = build();

    expect(wrapper.html()).toMatchSnapshot();
  });

  test("a user can add pages to the form", () => {
    const { wrapper, pages, createPageButton } = build();

    expect(pages()).toHaveLength(0);

    createPageButton().trigger("click");
    createPageButton().trigger("click");

    expect(pages()).toHaveLength(2);

    expect(
      pages()
        .at(0)
        .props()
    ).toEqual({
      uuid: wrapper.vm.pages[0].uuid,
      title: "New Page",
      items: []
    });
    expect(
      pages()
        .at(1)
        .props()
    ).toEqual({
      uuid: wrapper.vm.pages[1].uuid,
      title: "New Page",
      items: []
    });
  });

  test("a user can add sections to the form", () => {
    const userInfoPage = pageFactory.make();
    const { wrapper, pages } = build({
      overrideState: { pages: [userInfoPage] }
    });

    pages()
      .at(0)
      .vm.$emit("createSection", { pageId: userInfoPage.uuid });

    pages()
      .at(0)
      .vm.$emit("createSection", {
        pageId: userInfoPage.uuid,
        sectionId: wrapper.vm.pages[0].items[0].uuid
      });

    expect(wrapper.vm.pages[0].items[0]).toEqual(
      expect.objectContaining({
        type: "section",
        title: "New Section",
        items: [
          {
            type: "section",
            title: "New Section",
            items: [],
            uuid: wrapper.vm.pages[0].items[0].items[0].uuid
          }
        ]
      })
    );
  });

  test("a user can add questions to the form", () => {
    const userInfoPage = pageFactory.make();
    const { wrapper, pages } = build({
      overrideState: { pages: [userInfoPage] }
    });

    pages()
      .at(0)
      .vm.$emit("createQuestion", { pageId: userInfoPage.uuid });

    expect(wrapper.vm.pages[0].items[0]).toEqual(
      expect.objectContaining({
        type: "question",
        title: "New Question",
        responseType: "text"
      })
    );
  });

  test("a user can add questions to sub sections", () => {
    const userInfoPage = pageFactory.make();
    const { wrapper, pages } = build({
      overrideState: { pages: [userInfoPage] }
    });

    pages()
      .at(0)
      .vm.$emit("createQuestion", { pageId: userInfoPage.uuid });

    pages()
      .at(0)
      .vm.$emit("createSection", { pageId: userInfoPage.uuid });

    pages()
      .at(0)
      .vm.$emit("createQuestion", {
        pageId: userInfoPage.uuid,
        sectionId: wrapper.vm.pages[0].items[1].uuid
      });

    expect(wrapper.vm.pages[0].items[1]).toEqual(
      expect.objectContaining({
        type: "section",
        title: "New Section",
        items: [
          {
            type: "question",
            title: "New Question",
            responseType: "text",
            uuid: wrapper.vm.pages[0].items[1].items[0].uuid
          }
        ]
      })
    );
  });

  test("a user can edit a page title", () => {
    const userInfoPage = pageFactory.make();
    const { wrapper, pages } = build({
      overrideState: { pages: [userInfoPage] }
    });

    pages()
      .at(0)
      .vm.$emit("edit", {
        id: userInfoPage.uuid,
        data: { title: "Personal informations" }
      });

    expect(wrapper.vm.pages[0]).toEqual({
      ...userInfoPage,
      title: "Personal informations"
    });
  });

  test("a user can edit a section title", () => {
    const userInfoSection = sectionFactory.make();
    const userInfoPage = pageFactory.make({
      items: [userInfoSection]
    });
    const { wrapper, pages } = build({
      overrideState: { pages: [userInfoPage] }
    });

    pages()
      .at(0)
      .vm.$emit("edit", {
        id: userInfoSection.uuid,
        data: { title: "Personal informations section" }
      });

    expect(wrapper.vm.pages[0].items[0]).toEqual({
      ...userInfoSection,
      title: "Personal informations section"
    });
  });

  test("a user can edit a question title and type", () => {
    const ageQuestion = questionFactory.make();
    const userInfoPage = pageFactory.make({
      items: [ageQuestion]
    });
    const { wrapper, pages } = build({
      overrideState: { pages: [userInfoPage] }
    });

    pages()
      .at(0)
      .vm.$emit("edit", {
        id: ageQuestion.uuid,
        data: { title: "Whats your age?", responseType: "number" }
      });

    expect(wrapper.vm.pages[0].items[0]).toEqual({
      ...ageQuestion,
      title: "Whats your age?",
      responseType: "number"
    });
  });

  test("a user can delete a page", () => {
    const userInfoPage = pageFactory.make();
    const { wrapper, pages } = build({
      overrideState: {
        pages: [pageFactory.make(), userInfoPage, pageFactory.make()]
      }
    });

    expect(wrapper.vm.pages).toHaveLength(3);

    pages()
      .at(0)
      .vm.$emit("delete", userInfoPage.uuid);

    expect(wrapper.vm.pages).toHaveLength(2);
    expect(wrapper.vm.pages).not.toContain(userInfoPage);
  });

  test("a user can delete a section", () => {
    const userInfoSection = sectionFactory.make();
    const userInfoPage = pageFactory.make({
      items: [
        questionFactory.make(),
        sectionFactory.make(),
        userInfoSection,
        questionFactory.make()
      ]
    });
    const { wrapper, pages } = build({
      overrideState: {
        pages: [userInfoPage]
      }
    });

    expect(wrapper.vm.pages[0].items).toHaveLength(4);

    pages()
      .at(0)
      .vm.$emit("delete", userInfoSection.uuid);

    expect(wrapper.vm.pages[0].items).toHaveLength(3);
    expect(wrapper.vm.pages[0].items).not.toContain(userInfoSection);
  });

  test("a user can delete a question", () => {
    const ageQuestion = questionFactory.make();
    const userInfoPage = pageFactory.make({
      items: [
        questionFactory.make(),
        sectionFactory.make(),
        ageQuestion,
        questionFactory.make()
      ]
    });
    const { wrapper, pages } = build({
      overrideState: {
        pages: [userInfoPage]
      }
    });

    expect(wrapper.vm.pages[0].items).toHaveLength(4);

    pages()
      .at(0)
      .vm.$emit("delete", ageQuestion.uuid);

    expect(wrapper.vm.pages[0].items).toHaveLength(3);
    expect(wrapper.vm.pages[0].items).not.toContain(ageQuestion);
  });

  test("a user can export the created form", () => {
    const userFirstName = questionFactory.make({
      title: "What is your first name?",
      responseType: "text"
    });
    const userAge = questionFactory.make({
      title: "What is your age?",
      responseType: "number"
    });

    const userInformationSection = sectionFactory.make({
      items: [userFirstName, userAge]
    });

    const userPage = pageFactory.make({ items: [userInformationSection] });

    const { wrapper, exportButton } = build({
      overrideState: {
        pages: [userPage]
      }
    });

    expect(wrapper.vm.pages).toContain(userPage);

    exportButton().trigger("click");

    expect(exportButton().attributes().download).toEqual("form.json");
    expect(exportButton().attributes().href).toContain(userPage.uuid);
    expect(exportButton().attributes().href).toContain(
      userInformationSection.uuid
    );
    expect(exportButton().attributes().href).toContain(userFirstName.uuid);
    expect(exportButton().attributes().href).toContain(userAge.uuid);
  });

  test("a user can copy export form json to the clipboard", () => {
    const userFirstName = questionFactory.make({
      title: "What is your first name?",
      responseType: "text"
    });
    const userAge = questionFactory.make({
      title: "What is your age?",
      responseType: "number"
    });

    const userInformationSection = sectionFactory.make({
      items: [userFirstName, userAge]
    });

    const userPage = pageFactory.make({ items: [userInformationSection] });

    const { wrapper, copyExportButton } = build({
      overrideState: {
        pages: [userPage]
      }
    });

    expect(wrapper.vm.pages).toContain(userPage);

    copyExportButton().trigger("click");

    const copyToClipboardParam = copyToClipboard.mock.calls[0][0];

    expect(copyToClipboard).toHaveBeenCalled();
    expect(copyToClipboardParam).toContain(userPage.uuid);
    expect(copyToClipboardParam).toContain(userInformationSection.uuid);
    expect(copyToClipboardParam).toContain(userFirstName.uuid);
    expect(copyToClipboardParam).toContain(userAge.uuid);
  });

  test("a user can import a form from json text", () => {
    jest.useFakeTimers();

    const userFirstName = questionFactory.make({
      title: "What is your first name?",
      responseType: "text"
    });
    const userAge = questionFactory.make({
      title: "What is your age?",
      responseType: "number"
    });

    const userInformationSection = sectionFactory.make({
      items: [userFirstName, userAge]
    });

    const userPage = pageFactory.make({ items: [userInformationSection] });
    const form = formFactory.make({ items: [userPage] });

    const { wrapper, importField } = build();

    importField().element.value = JSON.stringify(form);
    importField().trigger("input");

    jest.advanceTimersByTime(1000);

    expect(wrapper.vm.pages).toEqual(form.items);
  });

  test("an error message is shown if import json string is wrong", () => {
    jest.useFakeTimers();

    const { importField, errorAlert } = build();

    expect(errorAlert().isVisible()).toBe(false);

    importField().element.value = "[";
    importField().trigger("input");

    jest.advanceTimersByTime(200);

    expect(errorAlert().isVisible()).toBe(false);

    jest.advanceTimersByTime(1000);

    expect(errorAlert().isVisible()).toBe(true);
    expect(errorAlert().text()).toContain("Invalid JSON format!");
  });
});
