import sectionFactory from "@/factories/sectionFactory";

describe("Factory: Section", () => {
  test("can generate a new page section", () => {
    expect(sectionFactory.make({ uuid: "secret-id" })).toEqual(
      expect.objectContaining({
        type: "section",
        title: "New Section",
        items: [],
        uuid: "secret-id"
      })
    );
  });
});
