import { shallowMount } from "@vue/test-utils";
import VQuestion from "@/components/VQuestion";
import questionFactory from "@/factories/questionFactory";

describe("VQuestion", () => {
  const build = props => {
    const wrapper = shallowMount(VQuestion, {
      propsData: {
        ...questionFactory.make(),
        ...props
      }
    });

    return {
      wrapper,
      responseTypeProp: () => wrapper.vm.$options.props.responseType,
      titleInput: () => wrapper.find('input[type="text"]'),
      typeInput: () => wrapper.find("select"),
      deleteButton: () => wrapper.find(".delete")
    };
  };

  it("renders main component", () => {
    const { wrapper } = build({ uuid: "123-abc-456" });

    expect(wrapper.html()).toMatchSnapshot();
  });

  test("a question can only accept 'text' or 'number' as 'responseType' prop", () => {
    const { responseTypeProp } = build();

    expect(
      responseTypeProp().validator &&
        responseTypeProp().validator("another-prop")
    ).toBeFalsy();
    expect(
      responseTypeProp().validator && responseTypeProp().validator("text")
    ).toBeTruthy();
    expect(
      responseTypeProp().validator && responseTypeProp().validator("number")
    ).toBeTruthy();
  });

  it("emits edit event when question title is changed", () => {
    jest.useFakeTimers();

    const { wrapper, titleInput } = build({ title: "My new question" });

    expect(titleInput().element.value).toEqual("My new question");

    titleInput().element.value = "Age";
    titleInput().trigger("input");

    jest.advanceTimersByTime(200);

    expect(wrapper.emitted().edit).toBeFalsy();

    jest.advanceTimersByTime(500);

    expect(wrapper.emitted().edit).toBeTruthy();
    expect(wrapper.emitted().edit[0]).toEqual([
      {
        id: wrapper.vm.uuid,
        data: {
          title: "Age"
        }
      }
    ]);
  });

  it("emits edit event when question type is changed", () => {
    const { wrapper, typeInput } = build({ responseType: "text" });

    expect(typeInput().element.value).toEqual("text");

    typeInput().element.value = "number";
    typeInput().trigger("change");

    expect(wrapper.emitted().edit).toBeTruthy();
    expect(wrapper.emitted().edit[0]).toEqual([
      {
        id: wrapper.vm.uuid,
        data: {
          responseType: "number"
        }
      }
    ]);
  });

  it("emits delete event when delete button is clicked", () => {
    const { wrapper, deleteButton } = build();

    deleteButton().trigger("click");

    expect(wrapper.emitted().delete).toBeTruthy();
    expect(wrapper.emitted().delete[0]).toEqual([wrapper.vm.uuid]);
  });
});
