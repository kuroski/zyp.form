import formFactory from "@/factories/formFactory";

describe("Factory: Form", () => {
  test("can generate a new form", () => {
    expect(formFactory.make({ uuid: "secret-id" })).toEqual(
      expect.objectContaining({
        type: "form",
        items: [],
        uuid: "secret-id"
      })
    );
  });
});
