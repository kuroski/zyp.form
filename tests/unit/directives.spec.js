import * as directives from "@/directives";

describe("directives", () => {
  describe("debounce", () => {
    test("a input can register for debounce change events", () => {
      jest.useFakeTimers();

      const element = {
        dispatchEvent: jest.fn(),
        oninput: jest.fn()
      };
      const binding = {
        value: 500
      };

      directives.debounce.bind(element, binding);

      expect(element.dispatchEvent).not.toHaveBeenCalled();

      element.oninput();
      element.oninput();

      jest.advanceTimersByTime(200);

      element.oninput();

      jest.advanceTimersByTime(500);

      expect(element.dispatchEvent).toHaveBeenCalledTimes(1);
    });

    test("a input must unbind it's listeners", () => {
      const element = {
        oninput: () => {}
      };

      directives.debounce.unbind(element);

      expect(element.oninput).toBeNull();
    });
  });
});
