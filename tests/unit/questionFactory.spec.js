import questionFactory from "@/factories/questionFactory";

describe("Factory: Question", () => {
  test("can generate a new page question", () => {
    expect(questionFactory.make({ uuid: "secret-id" })).toEqual(
      expect.objectContaining({
        type: "question",
        title: "New Question",
        responseType: "text",
        uuid: "secret-id"
      })
    );
  });
});
