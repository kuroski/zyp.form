import { shallowMount } from "@vue/test-utils";
import VSection from "@/components/VSection";
import VQuestion from "@/components/VQuestion";
import sectionFactory from "@/factories/sectionFactory";
import questionFactory from "@/factories/questionFactory";

describe("VSection", () => {
  const build = props => {
    const wrapper = shallowMount(VSection, {
      propsData: {
        ...sectionFactory.make(),
        ...props
      }
    });

    return {
      wrapper,
      createSectionButton: () => wrapper.find(".create-section"),
      sections: () =>
        wrapper.findAll(VSection).filter(item => item.classes("child")),
      createQuestionButton: () => wrapper.find(".create-question"),
      questions: () => wrapper.findAll(VQuestion),
      titleInput: () => wrapper.find('input[type="text"]'),
      deleteButton: () => wrapper.find(".delete")
    };
  };

  it("renders main component", () => {
    const { wrapper } = build({ uuid: "123-abc-456" });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it("emits addSection event when create section button is clicked", () => {
    const { wrapper, createSectionButton } = build();

    createSectionButton().trigger("click");

    expect(wrapper.emitted().createSection).toBeTruthy();
    expect(wrapper.emitted().createSection[0]).toEqual([wrapper.vm.uuid]);
  });

  test("a user can see sub sections", () => {
    const items = [
      sectionFactory.make({
        title: "Important Section",
        uuid: "38a92c85-4028-41c8-ab15-ad24f97cf33e"
      }),
      sectionFactory.make({
        title: "New Important Section",
        uuid: "ad24f97cf33e-4028-41c8-ab15-38a92c85"
      })
    ];
    const { sections } = build({ items });

    expect(sections()).toHaveLength(2);
    expect(
      sections()
        .at(0)
        .props().title
    ).toBe("Important Section");
    expect(
      sections()
        .at(1)
        .props().title
    ).toBe("New Important Section");
  });

  it("emits addQuestion event when create question button is clicked", () => {
    const { wrapper, createQuestionButton } = build();

    createQuestionButton().trigger("click");

    expect(wrapper.emitted().createQuestion).toBeTruthy();
    expect(wrapper.emitted().createQuestion[0]).toEqual([wrapper.vm.uuid]);
  });

  test("a user can see page questions", () => {
    const items = [
      questionFactory.make({
        uuid: "85211c02-0f39-4a31-a5a9-48c3a3b5d125",
        title: "What do you want?"
      }),
      questionFactory.make({
        uuid: "48c3a3b5d125-0f39-4a31-a5a9-85211c02",
        title: "Whats your age?"
      })
    ];
    const { questions } = build({ items });

    expect(questions()).toHaveLength(2);
    expect(
      questions()
        .at(0)
        .props().title
    ).toBe("What do you want?");
    expect(
      questions()
        .at(1)
        .props().title
    ).toBe("Whats your age?");
  });

  it("emits addSection event when create section button is clicked from a section", () => {
    const items = [
      sectionFactory.make({
        title: "Important Section",
        uuid: "38a92c85-4028-41c8-ab15-ad24f97cf33e"
      }),
      sectionFactory.make({
        title: "New Important Section",
        uuid: "ad24f97cf33e-4028-41c8-ab15-38a92c85"
      })
    ];
    const { wrapper, sections } = build({ items });

    sections()
      .at(0)
      .vm.$emit("createSection", items[0].uuid);

    expect(wrapper.emitted().createSection).toBeTruthy();
    expect(wrapper.emitted().createSection[0]).toEqual([items[0].uuid]);
  });

  it("emits addQuestion event when create question button is clicked from a section", () => {
    const items = [
      sectionFactory.make({
        title: "Important Section",
        uuid: "38a92c85-4028-41c8-ab15-ad24f97cf33e"
      }),
      sectionFactory.make({
        title: "New Important Section",
        uuid: "ad24f97cf33e-4028-41c8-ab15-38a92c85"
      })
    ];
    const { wrapper, sections } = build({ items });

    sections()
      .at(0)
      .vm.$emit("createQuestion", items[0].uuid);

    expect(wrapper.emitted().createQuestion).toBeTruthy();
    expect(wrapper.emitted().createQuestion[0]).toEqual([items[0].uuid]);
  });

  it("emits edit event when section title is changed", () => {
    jest.useFakeTimers();

    const { wrapper, titleInput } = build({ title: "My new section" });

    expect(titleInput().element.value).toEqual("My new section");

    titleInput().element.value = "User informations";
    titleInput().trigger("input");

    jest.advanceTimersByTime(200);

    expect(wrapper.emitted().edit).toBeFalsy();

    jest.advanceTimersByTime(500);

    expect(wrapper.emitted().edit).toBeTruthy();
    expect(wrapper.emitted().edit[0]).toEqual([
      {
        id: wrapper.vm.uuid,
        data: {
          title: "User informations"
        }
      }
    ]);
  });

  it("emits edit event when section is edited", () => {
    const items = [
      sectionFactory.make({
        title: "Important Section",
        uuid: "38a92c85-4028-41c8-ab15-ad24f97cf33e"
      }),
      sectionFactory.make({
        title: "New Important Section",
        uuid: "ad24f97cf33e-4028-41c8-ab15-38a92c85"
      })
    ];
    const { wrapper, sections } = build({ items });

    sections()
      .at(0)
      .vm.$emit("edit", {
        id: items[0].uuid,
        data: { title: "New section title" }
      });

    expect(wrapper.emitted().edit).toBeTruthy();
    expect(wrapper.emitted().edit[0]).toEqual([
      { id: items[0].uuid, data: { title: "New section title" } }
    ]);
  });

  it("emits delete event when delete button is clicked", () => {
    const { wrapper, deleteButton } = build();

    deleteButton().trigger("click");

    expect(wrapper.emitted().delete).toBeTruthy();
    expect(wrapper.emitted().delete[0]).toEqual([wrapper.vm.uuid]);
  });

  it("emits delete event when section is deleted", () => {
    const items = [
      sectionFactory.make({
        title: "Important Section",
        uuid: "38a92c85-4028-41c8-ab15-ad24f97cf33e"
      }),
      sectionFactory.make({
        title: "New Important Section",
        uuid: "ad24f97cf33e-4028-41c8-ab15-38a92c85"
      })
    ];
    const { wrapper, sections } = build({ items });

    sections()
      .at(0)
      .vm.$emit("delete", items[0].uuid);

    expect(wrapper.emitted().delete).toBeTruthy();
    expect(wrapper.emitted().delete[0]).toEqual([items[0].uuid]);
  });

  it("emits delete event when question is deleted", () => {
    const items = [
      questionFactory.make({
        title: "Important question",
        uuid: "38a92c85-4028-41c8-ab15-ad24f97cf33e"
      }),
      questionFactory.make({
        title: "New Important question",
        uuid: "ad24f97cf33e-4028-41c8-ab15-38a92c85"
      })
    ];
    const { wrapper, questions } = build({ items });

    questions()
      .at(0)
      .vm.$emit("delete", items[0].uuid);

    expect(wrapper.emitted().delete).toBeTruthy();
    expect(wrapper.emitted().delete[0]).toEqual([items[0].uuid]);
  });
});
