import mutations from "@/store/mutations";
import pageFactory from "@/factories/pageFactory";
import sectionFactory from "@/factories/sectionFactory";
import questionFactory from "@/factories/questionFactory";

describe("Store: mutations", () => {
  test("PAGE_CREATED", () => {
    const state = {
      pages: []
    };

    mutations.PAGE_CREATED(state);

    expect(state.pages).toHaveLength(1);
  });

  describe("SECTION_CREATED", () => {
    it("creates a section", () => {
      const state = {
        pages: [pageFactory.make()]
      };

      mutations.ITEM_CREATED(state, {
        pageId: state.pages[0].uuid,
        factory: sectionFactory
      });

      expect(state.pages[0].items).toHaveLength(1);
      expect(state.pages[0].items[0].type).toBe("section");
    });

    it("creates a question", () => {
      const state = {
        pages: [pageFactory.make()]
      };

      mutations.ITEM_CREATED(state, {
        pageId: state.pages[0].uuid,
        factory: questionFactory
      });

      expect(state.pages[0].items).toHaveLength(1);
      expect(state.pages[0].items[0].type).toBe("question");
    });

    it("creates a nested item", () => {
      const state = {
        pages: [pageFactory.make({ items: [sectionFactory.make()] })]
      };

      mutations.ITEM_CREATED(state, {
        pageId: state.pages[0].uuid,
        sectionId: state.pages[0].items[0].uuid,
        factory: questionFactory
      });

      expect(state.pages[0].items[0].items).toHaveLength(1);
      expect(state.pages[0].items[0].items[0].type).toBe("question");
    });
  });

  test("ITEM_EDITED", () => {
    const itemToEdit = questionFactory.make({
      title: "Old title",
      responseType: "text"
    });
    const state = {
      pages: [
        pageFactory.make({
          items: [sectionFactory.make({ items: [itemToEdit] })]
        })
      ]
    };

    mutations.ITEM_EDITED(state, {
      id: itemToEdit.uuid,
      data: { title: "New title", responseType: "number" }
    });

    expect(state.pages[0].items[0].items[0]).toEqual(
      expect.objectContaining({
        title: "New title",
        responseType: "number"
      })
    );
  });

  test("ITEM_DELETED", () => {
    const itemToDelete = questionFactory.make({
      title: "Old title",
      responseType: "text"
    });
    const state = {
      pages: [
        pageFactory.make({
          items: [sectionFactory.make({ items: [itemToDelete] })]
        })
      ]
    };

    mutations.ITEM_DELETED(state, itemToDelete.uuid);

    expect(state.pages[0].items[0].items).not.toContain(itemToDelete);
  });

  test("FORM_IMPORTED", () => {
    const pages = pageFactory.make({
      items: [sectionFactory.make()]
    });
    const state = {
      pages: []
    };

    mutations.FORM_IMPORTED(state, pages);

    expect(state.pages).toEqual(pages);
  });
});
