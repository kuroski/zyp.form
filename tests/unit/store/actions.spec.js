import actions from "@/store/actions";
import pageFactory from "@/factories/pageFactory";
import sectionFactory from "@/factories/sectionFactory";
import questionFactory from "@/factories/questionFactory";

describe("Store: actions", () => {
  test("PAGE_CREATED", () => {
    const commit = jest.fn();
    actions.PAGE_CREATED({ commit });
    expect(commit).toHaveBeenCalledWith("PAGE_CREATED");
  });

  test("SECTION_CREATED", () => {
    const commit = jest.fn();
    const expectedPayload = {
      pageId: "123-abc-456",
      sectionId: "456-cba-123",
      factory: sectionFactory
    };
    actions.SECTION_CREATED({ commit }, expectedPayload);
    expect(commit).toHaveBeenCalledWith("ITEM_CREATED", expectedPayload);
  });

  test("QUESTION_CREATED", () => {
    const commit = jest.fn();
    const expectedPayload = {
      pageId: "123-abc-456",
      sectionId: "456-cba-123",
      factory: questionFactory
    };
    actions.QUESTION_CREATED({ commit }, expectedPayload);
    expect(commit).toHaveBeenCalledWith("ITEM_CREATED", expectedPayload);
  });

  test("ITEM_EDITED", () => {
    const commit = jest.fn();
    const expectedPayload = {
      id: "123-abc-456",
      data: { title: "New title " }
    };
    actions.ITEM_EDITED({ commit }, expectedPayload);
    expect(commit).toHaveBeenCalledWith("ITEM_EDITED", expectedPayload);
  });

  test("ITEM_DELETED", () => {
    const commit = jest.fn();
    const id = "123-abc-456";
    actions.ITEM_DELETED({ commit }, id);
    expect(commit).toHaveBeenCalledWith("ITEM_DELETED", id);
  });

  test("FORM_IMPORTED", () => {
    const commit = jest.fn();
    const expectedPayload = [
      pageFactory.make({
        items: [questionFactory.make(), sectionFactory.make()]
      })
    ];
    actions.FORM_IMPORTED({ commit }, expectedPayload);
    expect(commit).toHaveBeenCalledWith("FORM_IMPORTED", expectedPayload);
  });
});
