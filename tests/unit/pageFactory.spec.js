import pageFactory from "@/factories/pageFactory";

describe("Factory: Page", () => {
  test("can generate a new page page", () => {
    expect(pageFactory.make({ uuid: "secret-id" })).toEqual(
      expect.objectContaining({
        type: "page",
        title: "New Page",
        items: [],
        uuid: "secret-id"
      })
    );
  });
});
