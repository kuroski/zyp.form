import * as helpers from "@/helpers";
import formFactory from "@/factories/formFactory";
import pageFactory from "@/factories/pageFactory";

describe("helpers", () => {
  it("classifies a component given a type", () => {
    expect(helpers.classifyComponent("section")).toBe("VSection");
    expect(helpers.classifyComponent("question")).toBe("VQuestion");
    expect(helpers.classifyComponent("unexisting-type")).toBeUndefined();
  });

  it("debounces a function", () => {
    jest.useFakeTimers();

    const expectedFn = jest.fn();
    const debounceInstance = helpers.debounce(expectedFn, 1000);

    debounceInstance(1);

    jest.advanceTimersByTime(200);

    debounceInstance(2);

    jest.advanceTimersByTime(500);

    debounceInstance(3);
    debounceInstance(4);

    jest.advanceTimersByTime(1000);

    expect(expectedFn).toHaveBeenCalledTimes(1);
    expect(expectedFn).toHaveBeenCalledWith(4);
  });

  it("generates a form json file base64", () => {
    const userPage = pageFactory.make({ uuid: "1234-abc-def-456" });
    const formString = helpers.encodeFormFile([userPage]);

    expect(formString).toContain("data:application/json;charset=utf-8,");
    expect(formString).toContain(userPage.uuid);
  });

  it("reads a file", async () => {
    const file = new File(["hello world"], "file.txt");
    const result = await helpers.readFile(file);
    expect(result).toBe("hello world");
  });

  describe("form json decoding", () => {
    test("a valid form json can be decoded", () => {
      const userPage = pageFactory.make({ uuid: "1234-abc-def-456" });
      const form = formFactory.make({
        uuid: "4321-bac-dqw-555",
        items: [userPage]
      });

      expect(helpers.decodeFormFile(form)).toEqual(form);
    });

    test("a valid form throws an exception", () => {
      const userPage = pageFactory.make({ uuid: "1234-abc-def-456" });
      const form = formFactory.make({
        uuid: "4321-bac-dqw-555",
        items: [userPage],
        title: "wrong attribute"
      });

      expect(helpers.decodeFormFile(form)).toThrowErrorMatchingSnapshot();
    });
  });
});
