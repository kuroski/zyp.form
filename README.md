# zyp.form

https://kuroski-zyp-form.netlify.com/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
// or
npm run test:unit
// or
npm run test:coverage
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Planning

The project consists in a basic form builder app.

We have three main funcionalities:

- Create a new form
- Export a form
- Import a form

Since I don't have stakeholders to know what is the main functionality of this app, I'll assume that **creating the form** it's the most important feature and it must be prioritized, followed by the **export** functionality and finnaly the **import**.

The form rendering is out of scope of this app.

### Form structure

The form consists in a JSON file that consists in:

- A form root element
- Form items called `Pages`
- Page items that can be
  - `Question`
  - `Section`

Every `Question` must have one of the following response types:

- text
- number

And `Pages` cannot contain other `Pages`.

#### Form properties

A form can have the following properties:

- `uuid` - an unique string identifier
- `type` - this value is fixed as "form"
- `items` - an array of `Pages`

#### Pages properties

A page can have the following properties:

- `uuid` - an unique string identifier
- `type` - this value is fixed as "page"
- `title` - a string title
- `items` - an array of `Questions` or `Sections`

#### Question properties

a question can have the following properties:

- `uuid` - an unique string identifier
- `type` - this value is fixed as "question"
- `title` - a string title
- `response_type` - an enum that can be "text" or "number"

#### Section properties

a section can have the following properties:

- `uuid` - an unique string identifier
- `type` - this value is fixed as "page"
- `title` - a string title
- `items` - an array of `Questions` or `Sections`

#### Minimum form json example

``` json
{
  "uuid": "ed0906ee-482b-41dd-bcb0-ab17608d084e",
  "type": "form",
  "items": [
    {
      "uuid": "ba3ca80d-549e-4e0d-b7eb-58300f011c34",
      "type": "page",
      "title": "First Page",
      "items": [
        {
          "type": "section",
          "title": "Important Section",
          "uuid": "38a92c85-4028-41c8-ab15-ad24f97cf33e",
          "items": [
            {
              "uuid": "85211c02-0f39-4a31-a5a9-48c3a3b5d125",
              "title": "What do you want?",
              "type": "question",
              "response_type": "text"
            }
          ]
        }
      ]
    }
  ]
}
```

## Next steps

In order to create the form builder, I don't need to modelate the store and think about how I'll import/export for now.

I can reach the initial goal of **creating the form builder component** by just passing a simple "finished" decoded object from the JSON sample as a prop and just build the required pieces to deliver the most important feature sooner.

But despite that, even if is written on the test instructions

> The application needs to use Vue.js and can use Vuex if appropriate

After implementing the component, I'll enter in a refactor phase to integrate Vuex just to prove knowledge.

After finishing this up, I'll implement the **export** followed by the **import** features.

## Extras

- Typeform/Paperform like form building
- i18n
- Improve performance by update the exact list location instead of mapping everything into a new object
- Refactor the test files
- Improve styling
- Create a markdown like parser and give the user the hability of creating forms by text, ex:

```
# User information page
  - Basic information section
    + Name: String
    + Age: Integer
  - Optional informations section
    - Sub optional informations section
      + some input text: String
```

This way we could create a parser and give the user this feature for rapid form creation.