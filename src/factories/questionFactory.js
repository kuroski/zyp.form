import uuid from "uuid/v1";

export default {
  make: overrideProps => ({
    type: "question",
    title: "New Question",
    uuid: uuid(),
    responseType: "text",
    ...overrideProps
  })
};
