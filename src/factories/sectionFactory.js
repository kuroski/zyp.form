import uuid from "uuid/v1";

export default {
  make: overrideProps => ({
    type: "section",
    title: "New Section",
    uuid: uuid(),
    items: [],
    ...overrideProps
  })
};
