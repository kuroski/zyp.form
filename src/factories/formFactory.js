import uuid from "uuid/v1";

export default {
  make: overrideProps => ({
    type: "form",
    uuid: uuid(),
    items: [],
    ...overrideProps
  })
};
