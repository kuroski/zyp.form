import uuid from "uuid/v1";

export default {
  make: overrideProps => ({
    type: "page",
    title: "New Page",
    uuid: uuid(),
    items: [],
    ...overrideProps
  })
};
