import {
  guard,
  string,
  constant,
  object,
  array,
  oneOf,
  either,
  mixed
} from "decoders";
import formFactory from "@/factories/formFactory";

export const classifyComponent = type => {
  return {
    section: "VSection",
    question: "VQuestion"
  }[type];
};

export const debounce = (fn, milliseconds) => {
  let timeout = null;

  return (...args) => {
    if (timeout) clearTimeout(timeout);

    timeout = setTimeout(() => {
      timeout = null;
      return fn(...args);
    }, milliseconds);
  };
};

export const encodeFormFile = (items = []) => {
  const form = JSON.stringify(formFactory.make({ items }));
  return `data:application/json;charset=utf-8,${encodeURIComponent(form)}`;
};

export const readFile = async file =>
  new Promise(resolve => {
    const reader = new FileReader();
    reader.onload = (() => e => resolve(e.target.result))();
    reader.readAsText(file);
  });

export const decodeFormFile = form => {
  const question = object({
    uuid: string,
    title: string,
    type: constant("question"),
    responseType: oneOf(["text", "number"])
  });

  const subSection = object({
    uuid: string,
    title: string,
    type: constant("section"),
    items: array(either(mixed, question))
  });

  const section = object({
    uuid: string,
    title: string,
    type: constant("section"),
    items: array(either(subSection, question))
  });

  const page = object({
    uuid: string,
    title: string,
    type: constant("page"),
    items: array(either(section, question))
  });

  const formDecoder = object({
    uuid: string,
    type: constant("form"),
    items: array(page)
  });

  const payloadGuard = guard(formDecoder);

  return payloadGuard(form);
};
