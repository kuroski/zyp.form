import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "form-builder",
      component: () =>
        import(/* webpackChunkName: "form-builder" */ "./views/FormBuilder.vue")
    }
  ]
});
