import * as helpers from "@/helpers";

export const debounce = {
  bind: (element, binding) => {
    if (binding.value === binding.oldValue) return;

    element.oninput = helpers.debounce(
      () => element.dispatchEvent(new Event("change")),
      parseInt(binding.value) || 500
    );
  },
  unbind: el => {
    el.oninput = null;
  }
};
