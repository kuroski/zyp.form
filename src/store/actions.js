import sectionFactory from "@/factories/sectionFactory";
import questionFactory from "@/factories/questionFactory";

export default {
  PAGE_CREATED: ({ commit }) => commit("PAGE_CREATED"),
  SECTION_CREATED: ({ commit }, payload) =>
    commit("ITEM_CREATED", { ...payload, factory: sectionFactory }),
  QUESTION_CREATED: ({ commit }, payload) =>
    commit("ITEM_CREATED", { ...payload, factory: questionFactory }),
  ITEM_EDITED: ({ commit }, payload) => commit("ITEM_EDITED", payload),
  ITEM_DELETED: ({ commit }, id) => commit("ITEM_DELETED", id),
  FORM_IMPORTED: ({ commit }, payload) => commit("FORM_IMPORTED", payload)
};
