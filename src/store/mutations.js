import Vue from "vue";
import pageFactory from "@/factories/pageFactory";

const searchAndCreateItem = (sectionId, factory) => item => {
  if (item.type === "question") return item;

  if (item.uuid === sectionId) item.items.push(factory.make());
  else item.items = item.items.map(searchAndCreateItem(sectionId, factory));

  return item;
};

const handleItemCreation = (page, factory, sectionId) => {
  if (!sectionId) return [...page.items, factory.make()];
  else return page.items.map(searchAndCreateItem(sectionId, factory));
};

const searchAndUpdate = (id, data) => item => {
  if (item.uuid === id) return { ...item, ...data };
  if (item.type === "question") return item;

  item.items = item.items.map(searchAndUpdate(id, data));
  return item;
};

export default {
  PAGE_CREATED: state => {
    state.pages.push(pageFactory.make());
  },
  ITEM_CREATED: (state, { pageId, sectionId, factory }) => {
    const pageIndex = state.pages.findIndex(page => page.uuid === pageId);

    Vue.set(
      state.pages[pageIndex],
      "items",
      handleItemCreation(state.pages[pageIndex], factory, sectionId)
    );
  },
  ITEM_EDITED: (state, { id, data }) => {
    state.pages = state.pages.map(searchAndUpdate(id, data));
  },
  ITEM_DELETED: (state, id) => {
    const findAndDelete = item => {
      if (item.uuid === id) return false;
      if (item.type === "question") return true;

      item.items = item.items.filter(findAndDelete);
      return item;
    };
    state.pages = state.pages.filter(findAndDelete);
  },
  FORM_IMPORTED: (state, payload) => {
    state.pages = payload;
  }
};
